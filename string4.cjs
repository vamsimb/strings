function string4(word = {}) {

  if (typeof(word) != "object" || Array.isArray(word) == true) {
    return {};
  }

  if(typeof(word) == "object" && word == null) {
    return {};
  }
  let result = []
  for (var key in word) {

    let temp = word[key].toLowerCase()
    temp = temp.replace(temp[0],temp[0].toUpperCase())
    result.push(temp);
    
  }
  if (result.length >0) return result.join(" ");
  return {};
}
    
module.exports = string4;          