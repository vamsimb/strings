function string2(word='') {

  if (typeof(word) !== 'string' || word.length == 0) return [];
  
  if ((word.split(".").length - 1) == 3 ) {
    const words = word.split(".");
    let result = [];
    for(let i in words) {
      if(isNaN(words[i]) || 0 > words[i] || words[i] >= 255 || (words[i] ==0 && words[i].length != 1)) return [];
      else result.push(Number(words[i]))
    }
    if(result.length == 4) return result;
  }
  return [];
}
module.exports = string2;