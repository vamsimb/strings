function string1(...word) {
  if(word.length == 0 || word == null || Array.isArray(...word)) {
    return 0
  }
  let result = [];
  for (let i of word) {
    i = i.replace("$","");
    i = i.replace(",","");
    if(Number(i)) result.push(Number(i))
    else result.push(0);
  }
  if(result.length != 0 )  return result
  return 0;
}
module.exports = string1;