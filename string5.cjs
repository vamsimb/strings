function string5(word =[]) {
  if(!Array.isArray(word) || word.length == 0) {
    return [];
  }
  if (word) return (word.join(" ") + '.');
  return []
}

module.exports = string5;