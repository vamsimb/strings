function string3(word='') {

  const monthNames = ["January", "February", "March", "April", "May", "June","July", "August", "September", "October", "November", "December"];

  if(word.length != 0 && typeof(word) === "string" && ((word.split("/").length -1 ) == 2) ) {

    word = word.split("/");

    if (0 < word[1] && word[1] <= 12) return monthNames[word[1] - 1];
  }
  return 0;
}


module.exports = string3;